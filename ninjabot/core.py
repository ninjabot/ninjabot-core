import logging
import os
import json
import re
import threading

import py_irc.irc as irc

logger = logging.getLogger(__name__)

class NinjaBot(irc.IRCClientServices):
	def __init__(self, channels, fifo, *args, **kwargs):
		self.channels = channels if isinstance(channels, list) else [channels]
		self.fifo = fifo
		super().__init__(*args, **kwargs)

	def loop(self):
		while True:
			fifo = open(self.fifo)
			data = ''
			while not data or data[-1] != '\n':
				data += fifo.read()
			commands = data.split('\n')
			for command in commands:
				try:
					command = json.loads(command)
				except:
					continue
				message = command['message']
				for chan in self.channels:
					if isinstance(chan, list):
						if re.match(chan[1], command['source']):
							chan = chan[0]
						else:
							continue
					logger.debug(f"{message} => {chan}")
					self.privmsg(chan, message)


	def on_loggedin(self, nick, as_target, as_nick, message, target, tags):
		try:
			os.remove(self.fifo)
		except FileNotFoundError:
			pass
		os.mkfifo(self.fifo)
		loop_thread = threading.Thread(target=self.loop)
		loop_thread.start()
		super().on_welcome(nick, message, target, tags)
		for chan in self.channels:
			if not isinstance(chan, str):
				chan = chan[0]
			self.join(chan)
