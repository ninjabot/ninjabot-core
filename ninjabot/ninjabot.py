import argparse
import logging
from sys import stdout

import yaml

from ninjabot.core import NinjaBot

logger = logging.getLogger()

def cli():
    parser = argparse.ArgumentParser("Ninjabot", description="Minimal irc bot for relaying messages")
    parser.add_argument("--config", type=str, required=False, default="ninjabot.conf")
    parser.add_argument("--verbose", action="store_true", required=False)
    args = parser.parse_args()

    config = yaml.load(open(args.config), Loader=yaml.FullLoader)["core"]

    console = logging.StreamHandler()
    logger.addHandler(console)

    if config.get("logfile") is not None:
        logger.addHandler(logging.FileHandler(config.get("logfile")))

    if args.verbose or config.get("verbose", False):
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    client = NinjaBot(
        config["channels"], config["fifo"],
        config["server"], config["nick"],
        tls=config.get("tls", True), tls_verify=config.get("tls_verify", True),
        account=config["account"], password=config["password"],
        client_cert=config.get("client_cert"), client_key=config.get("client_key"),
        threaded=config.get('threaded', False),
        port=config.get("port"),
        service=config.get("service", "NickServ"),
    )

    client.start()
