# How to
The systemd service available in the repository expect you to install ninjabot-core in a virtualenv in `/opt/ninjabot/env`,
and to have a `ninjabot` user and group on your system.

You can do it with the following on a debian system:
```
sudo adduser --system --home /opt/ninjabot ninjabot
sudo -u ninjabot python3 -m venv /opt/ninjabot/env
```
And then to really install ninjabot:
```
sudo -u ninjabot /opt/ninjabot/env/pip install git+https://gitlab.crans.org/ninja/ninjabot-core.git
sudo -u ninjabot cp /opt/ninjabot/env/lib/python<your version>/site-packages/ninjabot/ninjabot.conf.example /opt/ninjabot/ninjabot.conf
sudo cp /opt/ninjabot/env/lib/python<your version>/site-packages/ninjabot/ninjabot.service /etc/systemd/system/ninjabot.service
```
You can edit the config file you've copied in `/opt/ninjabot/ninjabot.conf` (documentation below).
And the start and enable the systemd service.
```
sudo systemctl start ninjabot.service
sudo systemctl enable ninjabot.service
```

# Configuration
```
core:
  nick: <bot nickname>
  server: <server ip>
  port: <server port>
  service: <nick service name, by default "NickServ">
  account: <NickServ acount name>
  password: <NickServ account password>
  channels: <list of chans that will receive notifications>
    - <chan1>
    - <chan2>
  # you can also define regex for each channel with:
  #  - [<chan3>, <regex on the provider name>]
  fifo: <path of the fifo, by default: "/tmp/ninjabot.fifo">
  tls: <enable tls>
  tls_verify: <verify server certificate>
  logfile: <path to logfile>
  verbose: <enable debug logs>
  client_cert: <path to the client certificate>
  client_key: <path to the client certificate private key>
```
